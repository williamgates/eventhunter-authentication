package com.eventhunter.eventhunterauthentication.EventhunterAuthenticationBeforeTests;

import com.eventhunter.eventhunterauthentication.model.ApplicationUser;
import com.eventhunter.eventhunterauthentication.repository.ApplicationUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class BeforeTests {
    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    private static boolean dataLoaded = false;
    @Before
    public void setUp() {
        if (!dataLoaded) {
            ApplicationUser applicationUser = new ApplicationUser(
                    "user",
                    "password",
                    "Name");
            applicationUserRepository.save(applicationUser);
            dataLoaded = true;
        }
    }

    @Test
    public void contextLoads() {
    }
}
