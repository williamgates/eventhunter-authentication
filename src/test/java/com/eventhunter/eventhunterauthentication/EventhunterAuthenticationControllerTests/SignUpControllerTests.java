package com.eventhunter.eventhunterauthentication.EventhunterAuthenticationControllerTests;

import com.eventhunter.eventhunterauthentication.security.EventHunterSecurity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class SignUpControllerTests {
    @Autowired
    private MockMvc mvc;

    @Test
    public void testMethodNotAllowed() throws Exception {
        mvc.perform(get("/user/sign-up"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUnauthorized() throws Exception {
        Map<String, String> map = new HashMap<>();
        String token =  "Bearer " + EventHunterSecurity.createJwt("user");
        mvc.perform(post("/user/sign-up").contentType(APPLICATION_JSON_UTF8)
                .header("Authorization", token)
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testBadRequest() throws Exception {
        Map<String, String> map;

        map = new HashMap<>();
        map.put("username", "sampleuser");
        mvc.perform(post("/user/sign-up").contentType(APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().is4xxClientError());

        map = new HashMap<>();
        map.put("username", "user");
        map.put("password", "password");
        mvc.perform(post("/user/sign-up").contentType(APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testSuccess() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("username", "sampleuser");
        map.put("password", "samplepassword");
        map.put("name", "Sample Name");
        mvc.perform(post("/user/sign-up").contentType(APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().isOk());
    }
}
