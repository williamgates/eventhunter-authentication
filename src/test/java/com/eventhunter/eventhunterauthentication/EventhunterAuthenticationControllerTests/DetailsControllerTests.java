package com.eventhunter.eventhunterauthentication.EventhunterAuthenticationControllerTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class DetailsControllerTests {
    @Autowired
    private MockMvc mvc;

    @Test
    public void testUsernameNotExists() throws Exception {
        Map<String, String> map = new HashMap<>();
        mvc.perform(get("/user/details/wrongusername"))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(map)));

    }

    @Test
    public void testUsernameExists() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("username", "user");
        map.put("name", "Name");
        mvc.perform(get("/user/details/" + map.get("username")))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(map)));
    }
}
