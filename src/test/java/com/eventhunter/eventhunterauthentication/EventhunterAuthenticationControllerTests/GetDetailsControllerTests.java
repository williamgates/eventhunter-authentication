package com.eventhunter.eventhunterauthentication.EventhunterAuthenticationControllerTests;

import com.eventhunter.eventhunterauthentication.model.ApplicationUser;
import com.eventhunter.eventhunterauthentication.repository.ApplicationUserRepository;
import com.eventhunter.eventhunterauthentication.security.EventHunterSecurity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class GetDetailsControllerTests {
    @Autowired
    private MockMvc mvc;

    @Test
    public void testNoToken() throws Exception {
        Map<String, String> map = new HashMap<>();
        mvc.perform(get("/user/get-details"))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(map)));
    }

    @Test
    public void testUsernameNotExists() throws Exception {
        Map<String, String> map = new HashMap<>();
        String token =  "Bearer " + EventHunterSecurity.createJwt("anotheruser");
        mvc.perform(get("/user/get-details")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(map)));
    }

    @Test
    public void testUsernameExists() throws Exception {
        Map<String, String> map = new HashMap<>();
        String token =  "Bearer " + EventHunterSecurity.createJwt("user");
        map.put("username", "user");
        map.put("name", "Name");
        mvc.perform(get("/user/get-details")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(map)));
    }
}
