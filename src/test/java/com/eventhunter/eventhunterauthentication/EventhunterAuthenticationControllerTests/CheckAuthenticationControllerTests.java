package com.eventhunter.eventhunterauthentication.EventhunterAuthenticationControllerTests;

import com.eventhunter.eventhunterauthentication.security.EventHunterSecurity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class CheckAuthenticationControllerTests {
    @Autowired
    private MockMvc mvc;

    @Test
    public void testAuthenticated() throws Exception {
        String token =  "Bearer " + EventHunterSecurity.createJwt("user");
        mvc.perform(get("/user/check-authentication")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    public void testNotAuthenticated() throws Exception {
        mvc.perform(get("/user/check-authentication"))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));
    }
}
