package com.eventhunter.eventhunterauthentication.EventhunterAuthenticationModelTests;

import com.eventhunter.eventhunterauthentication.model.ApplicationUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationUserTests {
    @Test
    public void testGetterSetter() throws Exception {
        String username = "sampleuser";
        String password = "samplepassword";
        String name = "Sample Name";
        ApplicationUser applicationUser = new ApplicationUser(username,
                password, name);

        Assert.assertTrue(applicationUser.getId() >= 0);
        Assert.assertEquals(applicationUser.getUsername(), username);
        Assert.assertEquals(applicationUser.getPassword(), password);
        Assert.assertEquals(applicationUser.getName(), name);

        String newUsername = "newsampleuser";
        String newPassword = "newsamplepassword";
        String newName = "New Sample Name";
        applicationUser.setUsername(newUsername);
        applicationUser.setPassword(newPassword);
        applicationUser.setName(newName);

        Assert.assertEquals(applicationUser.getUsername(), newUsername);
        Assert.assertEquals(applicationUser.getPassword(), newPassword);
        Assert.assertEquals(applicationUser.getName(), newName);
    }
}
