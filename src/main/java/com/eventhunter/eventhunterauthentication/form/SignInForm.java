package com.eventhunter.eventhunterauthentication.form;

public class SignInForm {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
