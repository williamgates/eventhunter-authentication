package com.eventhunter.eventhunterauthentication.security;

public class SecurityConstants {
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String ISSUER = "EventHunter";
    public static final String SECRET_KEY = "5gv+2^5(bbxewyc55rmio(0ial@$1-p6)$abg68#yx4v8z43kh";
}
