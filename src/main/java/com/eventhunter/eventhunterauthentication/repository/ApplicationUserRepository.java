package com.eventhunter.eventhunterauthentication.repository;

import com.eventhunter.eventhunterauthentication.model.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
    boolean existsByUsername(String username);
}
