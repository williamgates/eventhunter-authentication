package com.eventhunter.eventhunterauthentication.controller;

import com.eventhunter.eventhunterauthentication.form.SignInForm;
import com.eventhunter.eventhunterauthentication.form.SignUpForm;
import com.eventhunter.eventhunterauthentication.model.ApplicationUser;
import com.eventhunter.eventhunterauthentication.repository.ApplicationUserRepository;
import com.eventhunter.eventhunterauthentication.security.EventHunterSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class ApplicationUserController {
    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @GetMapping("/check-authentication")
    public ResponseEntity checkAuthentication(@RequestHeader(value = "Authorization", defaultValue = "") String token) {
        if (EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.ok().body("true");
        }
        return ResponseEntity.ok().body("false");
    }

    @GetMapping("/details/{username}")
    public Map<String, String> details(@PathVariable String username) {
        Map<String, String> map = new HashMap<>();
        if(!applicationUserRepository.existsByUsername(username)) {
            return map;
        }
        ApplicationUser applicationUser = applicationUserRepository.findByUsername(username);
        map.put("username", applicationUser.getUsername());
        map.put("name", applicationUser.getName());
        return map;
    }

    @GetMapping("/get-details")
    public Map<String, String> getDetails(@RequestHeader(value = "Authorization", defaultValue = "") String token) {
        Map<String, String> map = new HashMap<>();
        if (!EventHunterSecurity.verifyAuthentication(token)) {
            return map;
        }
        String username = EventHunterSecurity.getUsername(token);
        if(!applicationUserRepository.existsByUsername(username)) {
            return map;
        }
        ApplicationUser applicationUser = applicationUserRepository.findByUsername(username);
        map.put("username", applicationUser.getUsername());
        map.put("name", applicationUser.getName());
        return map;
    }

    @PostMapping("/sign-in")
    public ResponseEntity signIn(@RequestHeader(value = "Authorization", defaultValue = "") String token,
                                 @RequestBody SignInForm signInForm) {
        if (EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
        if (signInForm.getUsername() == null || signInForm.getPassword() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username and password cannot be empty.");
        }
        if (!applicationUserRepository.existsByUsername(signInForm.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username not found.");
        }
        ApplicationUser applicationUser = applicationUserRepository.findByUsername(signInForm.getUsername());
        if (!applicationUser.getPassword().equals(signInForm.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Wrong password.");
        }
        String jwt = EventHunterSecurity.createJwt(signInForm.getUsername());
        return ResponseEntity.ok().body("Bearer " + jwt);
    }

    @PostMapping("/sign-up")
    public ResponseEntity signUp(@RequestHeader(value = "Authorization", defaultValue = "") String token,
                                 @RequestBody SignUpForm signUpForm) {
        if (EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
        if (signUpForm.getUsername() == null || signUpForm.getPassword() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username and password cannot be empty.");
        }
        if (applicationUserRepository.existsByUsername(signUpForm.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username already exists.");
        }
        ApplicationUser applicationUser = new ApplicationUser(signUpForm.getUsername(), signUpForm.getPassword(), signUpForm.getName());
        applicationUserRepository.save(applicationUser);
        return ResponseEntity.ok().body(null);
    }
}
