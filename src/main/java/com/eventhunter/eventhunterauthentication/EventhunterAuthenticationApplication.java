package com.eventhunter.eventhunterauthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventhunterAuthenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventhunterAuthenticationApplication.class, args);
    }

}
